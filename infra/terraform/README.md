# Terraform Scripts for GCP OTXECM environment
This set of Terraform scripts will build out the following bits of infrastructure:
* A VPC network (takes the defaults) - this is needed for the Kubernetes cluster;
* A static External IP address for the ingress-nginx controller
* A 3-note Kubernetes cluster that uses the N2-standard-4 CPU;
* A DNS zone for my domain (I'm using hfconsulting.dev)
* A series of A-records for otds, otcs, etc in the cluster;

I didn't bother storing the terraform.tfstate file in a repo as this is a demo project. The assumption is
that when you run this, your Google Cloud environment has no infrastructure.  For a production
deployment, the tfstate file will be stored more securely. I haven't decided whether to use the Cloud storage
or to take advantage of a new feature on git hub for this

## Summary of files
* main.tf - main Terraform file. it calls all the others
* output.tf - scripts all the outputs that we need from this deployment to install the cluster;
* variables.tf - defines variables used in main script, gets populated from terraform.tfvars
* terraform.tfvars - values I use in my deployment (YMMV);
* dnsrec/dnsrec.tf - DNS record module to build A records
* k8s/k8s.tf - module to build Kubernetes cluster
* vpc/vpc.tf - module to build VPC

## Deployment
* You will need to install Terraform for your environment first;
* Create a service account in GCP
    * Select the project you are going to use;
    * Go to IAM-> Service account and "Create Service Account"
    * Give it any name and click create
    * for the role, choose Project->Editor;
    * Skip granting other access and click Done;
    * After it is created, download the service account key (as JSON)
    * Save the key JSON to this file.  In terraform.tfvars, set the param configuration_file 
    to this parameter.
* From this directory run terraform:
    * terraform init;
    * terraform plan;
    * terraform apply;
* this will take about 10 minutes
