output "VPC-Network-Name" {
    value = module.vpcmodule.vpc_name
}

output "Static-External-IP" {
    value = google_compute_address.ext-ip.address
}

output "DNS-NameServers" {
    value = google_dns_managed_zone.otxecm-zone.name_servers
}