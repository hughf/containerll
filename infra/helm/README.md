## Helm files to be copied to our deployment
This directory is for any files from the core otxecm helm chart that are updated for this deployment.
Typically this would be things like the values.yaml or Chart.yaml if they are updated from the original,
but more likely the platform/<provider>.yaml file, in this case, gcp.yaml

Any file in this directory will be copied to otxecm/