#!/bin/sh
WORKDIR=`pwd`
. ${WORKDIR}/.env 
if [ $# -eq 0 ]
  then
 IPADDR=$(gcloud compute addresses list | grep ${GCP_EXT_IP} | awk {'print $2'})
else 
  IPADDR=$1 
fi

if [ -z "$IPADDR" ] 
  then
     echo no IP ADDRESS found. Check that external IP was created using 
     echo the label ${GCP_EXT_IP}
     exit 0
fi 

# When I have time, need to get IP address using this:
# gcloud compute addresses list | grep hfc-ext-ip | awk {'print $2'}
#
#IPADDR=$1 
HELMARGS="--set rbac.create=true --set controller.service.loadBalancerIP=${IPADDR} --set controller.config.proxy-body-size=1024" 
echo Using IP ADdress ${IPADDR}
helm install otxecm-ingress ingress-nginx/ingress-nginx ${HELMARGS}  
