#!/bin/bash
# This script removes secrets we created. 
# Consider it part of cluster tear-down
# It only deletes secrets created in setup-secret.sh

# This secret name is defined in infra/k8s/otcluster-secret.yaml
OTXECM_SECRET=otxecm-secrets
# This secret name is defined in otxecm/platform/gcp.yaml
DOCKER_SECRET=hfc-docker
# This secret name is defined in infra/k8s/xecm-secret.yaml and otxecm/platform/gcp.yaml
TLS_SECRET=xecm-secret
echo Deleting secret ${OTXECM_SECRET}
kubectl delete secret ${OTXECM_SECRET}

echo Deleting Docker seceret ${DOCKER_SECRET}
kubectl delete secret ${DOCKER_SECRET}

echo Deleting TLS secret ${TLS_SECRET}
kubectl delete secret ${TLS_SECRET}
