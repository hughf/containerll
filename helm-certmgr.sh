#!/bin/sh
WORKDIR=`pwd`
K8SDIR=${WORKDIR}/infra/k8s
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --set installCRDs=true

echo Deploying cluster issuer to kubernetes
kubectl create -f ${K8SDIR}/cluster-issuer-nginx.yaml