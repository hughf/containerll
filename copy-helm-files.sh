#!/bin/bash
WORKDIR=`pwd`
SRCDIR=${WORKDIR}/infra/helm
DSTDIR=${WORKDIR}/otxecm

for srcObj in ${SRCDIR}/*
do
    filename=$(basename -- "$srcObj")
    if [ $filename == "README.md" ] ; then
        echo "Skipping readme file..."
        continue
    fi
    echo ${filename}
    echo "copying ${srcObj}"
    cp -r ${srcObj} ${DSTDIR}
done